require AlembicWrapper, FileIO;

/// Get the PolygonMesh at the given path and its associated transform matrix as a Mat44
function ArnoldUtilAlembicGetMeshAndXform(io AlembicArchiveReader archive, String path, io PolygonMesh mesh, io Mat44 xform)
{
   AlembicPolyMeshReader poly_mesh_reader = archive.getPolyMesh(path);
   poly_mesh_reader.readSample(0.0, mesh);
   
   // split the path into tokens
   // then rebuild the path, concatenating transforms as we go
   String toks[] = path.split('/');
   String curpath = '';
   for (UInt32 j=0; j < toks.size(); ++j)
   {
      if (toks[j] == "") continue;
      curpath += '/' + toks[j];
      AlembicXformReader xform_reader = archive.getXform(curpath);
      if (xform_reader)
      {
         if (xform_reader.valid())
         {  
            xform *= xform_reader.readSample(0.0).toMat44();
         }
      }
   }
}

/// Load an Alembic file and spit out all geo to Arnold
/// \param Filename Path to the ALembic .abc to load
/// \param shader A shader to attach to all the geo. If not initialized, this will be ignored
/// \param meshes Will be filled with the names of all created polymesh nodes
function ArnoldUtilAlembicLoad(String filename, AtNode shader, io String polymesh_nodes[])
{
   FilePath path = FilePath(filename).expandEnvVars();

   if (!path.exists())
   {
      setError("File '" + path.string() + "' does not exist");
      return;
   }

   AlembicArchiveReader archive(path.string());
   if (archive.valid())
   {
      String poly_mesh_paths[] = archive.getPathsOfType('PolyMesh');
      for (UInt32 i=0; i < poly_mesh_paths.size(); ++i)
      {
         
         String name = poly_mesh_paths[i];

         PolygonMesh mesh = PolygonMesh();
         Mat44 xform;
         ArnoldUtilAlembicGetMeshAndXform(archive, name, mesh, xform);

         // Create the Arnold polymesh from the Fabric PolygonMesh
         AtNode mesh_node = ArnoldUtilConvertPolygonMesh(mesh, name);
         
         if (mesh_node.ptr)
         {
            // Set the xform matrix
            xform = xform.transpose(); //< Fabric uses the opposite convention to Arnold
            AiNodeSetMatrix(mesh_node, "matrix", xform);

            // push the name of the node onto the array
            polymesh_nodes.push(name);

            // create a shader
            if (shader.ptr)
            {
               AiNodeSetPtr(mesh_node, "shader", shader.ptr);
            }
         }
         else
         {
            setError("Failed to read mesh '" + name + "' from Alembic archive '" + path.string() + "'");
         }
      }
   }
   else
   {
      setError("Could not open Alembic file '" + path.string() + "'");
   }
}


/// Write the current universe to an Alembic archive
/// \param filename Path to the archive to write
/// \param mask Node mask to apply to the write operation
/// \param nodes List of nodes to write. If empty, the entire universe will be written (filtered by mask)
function ArnoldUtilAlembicWrite(String filename, Integer mask, String nodes[])
{
   
   FilePath path = FilePath(filename).expandEnvVars();
   AlembicArchiveWriter archive(path.string());
   if (!archive.valid())
   {
      setError("Could not create Alembic archive '" + path.string() + "'");
      return;
   }
}

